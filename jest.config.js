export default {
  roots: ["<rootDir>/src"],
  testEnvironment: "jest-environment-node",
  testMatch: ["**/__tests__/**/*.+(ts|tsx)", "**/?(*.)+(spec|test).+(ts|tsx)"],
  transform: {
    "^.+\\.(ts|tsx)$": "ts-jest"
  },
  globals: {
    "ts-jest": {
      babelConfig: {
        plugins: ["@babel/plugin-transform-modules-commonjs", "babel-plugin-transform-import-meta"]
      }
    }
  },
  moduleNameMapper: {
    "^(\\.\\.?\\/.*)\\.js$": "$1"
  }
};
