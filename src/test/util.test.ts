import { strict as assert } from 'assert';
import { caseInsensitiveCompare } from '../util.js';

test('Exact text should be equal', () => {
  assert(caseInsensitiveCompare('ABC', 'ABC') === 0);
  assert(caseInsensitiveCompare('def', 'def') === 0);
  assert(caseInsensitiveCompare('', '') === 0);
  assert(caseInsensitiveCompare(' A1_', ' A1_') === 0);
});

test('Differing case only should be equal', () => {
  assert(caseInsensitiveCompare('AbC', 'aBc') === 0);
  assert(caseInsensitiveCompare('def', 'DEF') === 0);
  assert(caseInsensitiveCompare(' x2=', ' X2=') === 0);
});

test('Different text should be different', () => {
  assert(caseInsensitiveCompare('AAA', 'AAB') !== 0);
  assert(caseInsensitiveCompare('aaa1', 'aaa2') !== 0);
  assert(caseInsensitiveCompare('a', 'a ') !== 0);
  assert(caseInsensitiveCompare('', '.') !== 0);
});

test('Text should sort correctly', () => {
  const text = ['b4', 'a2', 'A3', 'A1', 'B5', 'b6']
    .sort(caseInsensitiveCompare)
    .join(',');
  assert(text === 'A1,a2,A3,b4,B5,b6');
});
