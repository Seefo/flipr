export const caseInsensitiveCompare = (a: string, b: string): number =>
  a.toLowerCase().localeCompare(b.toLowerCase());
